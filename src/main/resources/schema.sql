CREATE TABLE user_info (
  user_info_id int(11) NOT NULL AUTO_INCREMENT,
  sec_user_id int(11) NOT NULL,
  first varchar(100) DEFAULT '',
  last varchar(100) DEFAULT '',
  PRIMARY KEY (user_info_id)
) ;


CREATE TABLE user_info_item (
  user_info_item_id int(11) NOT NULL AUTO_INCREMENT,
  user_info_id int(11) NOT NULL,
  user_info_item_type int(2),
  user_info_item_value varchar(250) DEFAULT '',
  PRIMARY KEY (user_info_item_id),
  FOREIGN KEY (user_info_id)  references user_info(user_info_id)  on delete cascade
) ;


CREATE TABLE sec_user (
  sec_user_id int(11) NOT NULL AUTO_INCREMENT,
  sec_user_name varchar(50) DEFAULT '',
  user_password  varchar (100),
  PRIMARY KEY (sec_user_id)
) ;


CREATE TABLE sec_group (
  sec_group_id int(11) NOT NULL AUTO_INCREMENT,
  sec_group_name varchar(50) DEFAULT '',
  description  varchar (255),
  account_only_flag boolean,
    PRIMARY KEY (sec_group_id)
) ;


CREATE TABLE sec_group_user (
  sec_group_user_id int(11) NOT NULL AUTO_INCREMENT,
  sec_group_id int(11) NOT NULL,
  sec_user_id int(11) NOT NULL,
  PRIMARY KEY (sec_group_user_id),
 FOREIGN KEY (sec_user_id)  references sec_user(sec_user_id),
 FOREIGN KEY (sec_group_id)  references sec_group(sec_group_id)
) ;


CREATE TABLE sec_authority (
  sec_authority_id int(11) NOT NULL AUTO_INCREMENT,
  sec_authority_name varchar(50) NOT NULL,
  description  varchar (255),
  PRIMARY KEY (sec_authority_id)
) ;


CREATE TABLE sec_authority_user (
  sec_authority_user_id int(11) NOT NULL AUTO_INCREMENT,
  sec_authority_id int(11) NOT NULL,
  sec_user_id int(11) NOT NULL,
  PRIMARY KEY (sec_authority_user_id),
    FOREIGN KEY (sec_authority_id)  references sec_authority(sec_authority_id),
  FOREIGN KEY (sec_user_id)  references sec_user(sec_user_id)
) ;


CREATE TABLE sec_authority_group (
  sec_authority_group_id int(11) NOT NULL AUTO_INCREMENT,
  sec_authority_id int(11) NOT NULL,
  sec_group_id int(11) NOT NULL,
  PRIMARY KEY (sec_authority_group_id),
  FOREIGN KEY (sec_authority_id)  references sec_authority(sec_authority_id),
  FOREIGN KEY (sec_group_id)  references sec_group(sec_group_id)
) ;




CREATE TABLE sec_account (
  sec_account_id int(11) NOT NULL AUTO_INCREMENT,
  sec_account_name varchar(50) NOT NULL,
  description  varchar (255),
  PRIMARY KEY (sec_account_id)
) ;



CREATE TABLE sec_account_user (
  sec_account_user_id int(11) NOT NULL AUTO_INCREMENT,
  sec_account_id int(11) NOT NULL,
  sec_user_id int(11) NOT NULL,
  PRIMARY KEY (sec_account_user_id),
  FOREIGN KEY (sec_account_id)  references sec_account(sec_account_id),
  FOREIGN KEY (sec_user_id)  references sec_user(sec_user_id)
) ;


CREATE TABLE sec_account_group (
  sec_account_group_id int(11) NOT NULL AUTO_INCREMENT,
  sec_account_id int(11) NOT NULL,
  sec_group_id int(11) NOT NULL,
  PRIMARY KEY (sec_account_group_id),
  FOREIGN KEY (sec_account_id)  references sec_account(sec_account_id),
  FOREIGN KEY (sec_group_id)  references sec_group(sec_group_id)
) ;


create view sec_user_authority_vw as
  select u.sec_user_id, u.sec_user_name, sa.sec_authority_id, sa.sec_authority_name
  from sec_user u, sec_group_user gu, sec_authority_group ag, sec_authority sa
  where
    sa.sec_authority_id = ag.sec_authority_id
    and ag.sec_group_id =  gu.sec_group_id
    and gu.sec_user_id = u.sec_user_id
  union
  select u.sec_user_id, u.sec_user_name, sa.sec_authority_id, sa.sec_authority_name
  from sec_user u,  sec_authority_user au, sec_authority sa
  where
    sa.sec_authority_id = au.sec_authority_id
    and au.sec_user_id = u.sec_user_id;




