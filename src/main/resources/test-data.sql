INSERT INTO sec_user (sec_user_id, sec_user_name) VALUES (71, 'skjenco');
INSERT INTO sec_user (sec_user_id, sec_user_name) VALUES (72, 'karen');
INSERT INTO sec_user (sec_user_id, sec_user_name) VALUES (73, 'parker');
INSERT INTO sec_user (sec_user_id, sec_user_name) VALUES (74, 'mckenna');

INSERT INTO user_info (user_info_id, sec_user_id, first, last) VALUES (71, 71, 'Stephen', 'Jensen');
INSERT INTO user_info (user_info_id, sec_user_id, first, last) VALUES (72, 72, 'Karen', 'Jensen');
INSERT INTO user_info (user_info_id, sec_user_id, first, last) VALUES (73, 73, 'Parker', '');
INSERT INTO user_info (user_info_id, sec_user_id, first, last) VALUES (74, 74, 'McKenna', 'Jensen');

INSERT INTO sec_group (sec_group_id, sec_group_name) VALUES (71, 'basic');
INSERT INTO sec_group (sec_group_id, sec_group_name) VALUES (72, 'admin');
INSERT INTO sec_group ( sec_group_name) VALUES ('developer');

INSERT INTO sec_group_user (sec_group_user_id, sec_group_id, sec_user_id) VALUES (71, 72, 71);
INSERT INTO sec_group_user (sec_group_user_id, sec_group_id, sec_user_id) VALUES (74, 71, 72);
INSERT INTO sec_group_user (sec_group_user_id, sec_group_id, sec_user_id) VALUES (75, 71, 73);

INSERT INTO sec_authority (sec_authority_id, sec_authority_name) VALUES (71, 'ROLE_USER');
INSERT INTO sec_authority (sec_authority_id, sec_authority_name) VALUES (72, 'main_update_self_button');
INSERT INTO sec_authority (sec_authority_id, sec_authority_name) VALUES (73, 'main_add_user_button');
INSERT INTO sec_authority (sec_authority_id, sec_authority_name) VALUES (74, 'main_see_stats');

INSERT INTO sec_account (sec_account_id, sec_account_name, description) VALUES (1, 'sensitiveAccounts', 'Priority accounts experiencing issues');

INSERT INTO sec_account_group (sec_account_group_id, sec_account_id, sec_group_id) VALUES (1, 1, 72);

INSERT INTO sec_account_user (sec_account_user_id, sec_account_id, sec_user_id) VALUES (1, 1, 72);

INSERT INTO sec_authority_group (sec_authority_group_id, sec_authority_id, sec_group_id) VALUES (71, 72, 72);
INSERT INTO sec_authority_group (sec_authority_group_id, sec_authority_id, sec_group_id) VALUES (73, 71, 72);
INSERT INTO sec_authority_group (sec_authority_group_id, sec_authority_id, sec_group_id) VALUES (79, 73, 72);
INSERT INTO sec_authority_group (sec_authority_group_id, sec_authority_id, sec_group_id) VALUES (82, 74, 72);
INSERT INTO sec_authority_group (sec_authority_group_id, sec_authority_id, sec_group_id) VALUES (83, 72, 71);

INSERT INTO sec_authority_user (sec_authority_user_id, sec_authority_id, sec_user_id) VALUES (71, 74, 73);
