package com.iskj.model;

import javax.persistence.*;

@Entity
@Table(name="sec_user")
public class SecUser {

    @Id
    @GeneratedValue
    @Column(name="sec_user_id")
    protected Integer id;


    @Column(name="sec_user_name")
    protected String userName;

    @Column(name="user_password")
    protected  String userPassword;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
