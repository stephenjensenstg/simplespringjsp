package com.iskj.services;

import com.iskj.model.SecUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("myTestService")
public class MyTestServiceImpl implements MyTestService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @PersistenceContext
    EntityManager em;

    public String test(String str){

        List<Map<String, Object>> maps = jdbcTemplate.queryForList("Select * from sec_user_authority_vw");

        SecUser secUser = em.find(SecUser.class, 72);

        String retval = maps.stream().map(Object::toString).collect(Collectors.joining(":"));

        return "my return value " + retval;
    }
}
